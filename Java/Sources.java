
public class Sources {
	public static final String GIF_BOAT = "Boat.gif";
	public static final String PNG_BOAT = "Boat.png";
	
	public static final String GIF_KANN = "Kann.gif";
	public static final String PNG_KANN = "Kann.png";
	public static final String GIF_KANN_ANGEL = "Kann_Angel.gif";
	public static final String PNG_KANN_ANGEL = "Kann_Angel.png";
	public static final String GIF_KANN_BACK = "Kann_Back.gif";
	public static final String PNG_KANN_BACK = "Kann_Back.png";
	public static final String GIF_KANN_LAUGH = "Kann_Laughing.gif";
	public static final String PNG_KANN_LAUGH = "Kann_Laughing.png";
	
	public static final String JPG_LANDSCAPE = "Landscape.jpg";

	public static final String GIF_MISS = "Miss.gif";
	public static final String PNG_MISS = "Miss.png";
	public static final String GIF_MISS_DEAD_1 = "Miss_Dead_1.gif";
	public static final String PNG_MISS_DEAD_1 = "Miss_Dead_1.png";
	public static final String GIF_MISS_DEAD_2 = "Miss_Dead_2.gif";
	public static final String PNG_MISS_DEAD_2 = "Miss_Dead_2.png";
	public static final String GIF_MISS_DEAD_3 = "Miss_Dead_3.gif";
	public static final String PNG_MISS_DEAD_3 = "Miss_Dead_3.png";
	public static final String GIF_MISS_DEAD_4 = "Miss_Dead_4.gif";
	public static final String PNG_MISS_DEAD_4 = "Miss_Dead_4.png";
	public static final String GIF_MISS_LOOK_LEFT = "Miss_Looking_Left.gif";
	public static final String PNG_MISS_LOOK_LEFT = "Miss_Looking_Left.png";
	public static final String GIF_MISS_LOOK_RIGHT = "Miss_Looking_Right.gif";
	public static final String PNG_MISS_LOOK_RIGHT = "Miss_Looking_Right.png";
	public static final String GIF_MISS_SHOCKED = "Miss_Shocked.gif";
	public static final String PNG_MISS_SHOCKED = "Miss_Shocked.png";
	
	public static final String WAV_OMG = "OMG.wav";
	
	public static final String TTF_TIMES_NEW_ROMAN = "times.ttf";
	
	public static final String STRING_OMG_1 = "OMG,";
	public static final String STRING_OMG_2 = "OMG, they";
	public static final String STRING_OMG_3 = "OMG, they killed";
	public static final String STRING_OMG_4 = "OMG, they killed Kenny!";
	public static final String STRING_OMG_5 = "You bustards!";
}
