import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;


@SuppressWarnings("serial")
public class AnimationFrame extends JFrame implements ActionListener, Runnable {

	// Verzoegerung zwischen den repaint()-Aufrufen
	protected static int DELAY = 4;
	// Koordinaten der Ufer (links, rechts)
	protected static final Pair<Integer,Integer> BANK_LEFT = new Pair<Integer,Integer>(250,430);
	protected static final Pair<Integer,Integer> BANK_RIGHT = new Pair<Integer,Integer>(650,430);
	
	// ausgerechnete Zustaende des linken Ufers - von der Klasse Missionare
	protected ComparableList<SetTriple> mPath;
	
	// Referenz zu einem DrawPanel, einem Boot, Missionaren und Kannibalen
	protected DrawPanel mPanel;
	protected Boat mBoat;
	protected Missionar[] mMiss;
	protected Kannibale[] mKann;
	
	// Laufvariable fuer mPath
	protected int count = 1;
	
	
	//***** Fuer das Arbeiten mit den Buttons *****//
	
	// separater neuer Thread
	protected Thread mView = new Thread(this);
	// Flag, ob der Button Run gedrueckt wurde
	protected boolean running;
	// Flag fuer die Methode run() des Threads, ob das Programm weiterlaufen soll
	protected boolean loop = true;
	// Buttons - Step, Run, Stop, Reset, End
	protected JButton step = new JButton("Step");
	protected JButton run = new JButton("Run");
	protected JButton stop = new JButton("Stop");
	protected JButton reset = new JButton("Reset");
	protected JButton end = new JButton("End");
	// TextField fuer das DELAY
	protected JTextField delayField = new JTextField();
	
	
	public AnimationFrame(ComparableList<SetTriple> path) {
		
		// initialisiere Variablen
		mPath = path;
		
		mPanel = new DrawPanel(1000,750);
		mBoat = mPanel.mBoat;
		mMiss = mPanel.mMiss;
		mKann = mPanel.mKann;
		
		// initialisiere Buttons und das Button-Panel
		this.step.setPreferredSize(new Dimension(100,20));
		this.step.addActionListener(this);
		this.step.setMnemonic('S');
		
		this.run.setPreferredSize(new Dimension(100,20));
		this.run.addActionListener(this);
		this.run.setMnemonic('R');
		
		this.stop.setPreferredSize(new Dimension(100,20));
		this.stop.addActionListener(this);
		this.stop.setMnemonic('P');
		
		this.reset.setPreferredSize(new Dimension(100,20));
		this.reset.addActionListener(this);
		this.reset.setMnemonic('T');
		
		this.end.setPreferredSize(new Dimension(100,20));
		this.end.addActionListener(this);
		this.end.setMnemonic('E');
		
		JPanel buttons = new JPanel();
		buttons.setPreferredSize(new Dimension(1000, 25));
		buttons.setLayout(new BoxLayout(buttons, BoxLayout.LINE_AXIS));
		buttons.add(Box.createHorizontalGlue());
		buttons.add(this.step);
		buttons.add(Box.createHorizontalGlue());
		buttons.add(this.run);
		buttons.add(Box.createHorizontalGlue());
		buttons.add(this.stop);
		buttons.add(Box.createHorizontalGlue());
		buttons.add(this.reset);
		buttons.add(Box.createHorizontalGlue());
		buttons.add(this.end);
		buttons.add(Box.createHorizontalGlue());
		
		// initialisiere TextField und TextField-Panel 
		this.delayField.addActionListener(this);
		
		JPanel bottom = new JPanel();
		bottom.setPreferredSize(new Dimension(1000, 25));
		bottom.setLayout(new BoxLayout(bottom, BoxLayout.LINE_AXIS));
		bottom.add(Box.createHorizontalGlue());
		bottom.add(this.delayField);
		bottom.add(Box.createHorizontalGlue());
		
		// erzeuge AnimationFrame
		setTitle("Missionare vs Kannibalen");
		setSize(1000,775);
		setPreferredSize(new Dimension(1000,800));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		add(buttons, BorderLayout.NORTH);
		add(mPanel, BorderLayout.CENTER);
		add(bottom, BorderLayout.SOUTH);
		pack();
		setVisible(true);
	}
	
	// Bewegt die Person um 1px
	protected void move_step(MyObject obj, Pair<Integer, Integer> goal) {
		int m = (goal.getSecond()-obj.getActual().getSecond())/(goal.getFirst()-obj.getActual().getFirst());
		int b = obj.getActual().getSecond() - m*obj.getActual().getFirst();
		int x;
		if (obj.getStatus() == MyObject.LEFT) {
			x = obj.getActual().getFirst() + 1;
		} else {
			x = obj.getActual().getFirst() - 1;
		}
		int y = m*x + b;
		obj.setActual(x, y);
	}

	// Bewegt die Person bis zum Zielpunkt
	protected void move(MyObject person, Pair<Integer, Integer> goal) {
		while(person.getActual().getFirst().compareTo(goal.getFirst()) != 0) {
			move_step(person, goal);
			refresh(); 
		}
	}
	
	// Bewegt die Person senkrecht nach oben
	protected void moveToHeaven(MyObject person, Pair<Integer, Integer> goal) {
		while(person.getActual().getSecond().compareTo(goal.getSecond()) != 0) {
			person.setActual(person.getActual().getFirst(), person.getActual().getSecond()-1);
			refresh(); 
		}
	}
	
	// Bewegt Person vom Ufer in das Boot
	protected void toBoat(Person person) {
		if (person.getStatus() != mBoat.getStatus()) {
			JOptionPane.showMessageDialog(null, "Keine Person verfuegbar!", "Fehler", JOptionPane.ERROR_MESSAGE);
		}
		if (person.getStatus() == MyObject.LEFT) {
			move(person, BANK_LEFT);
		} else {
			move(person, BANK_RIGHT);
		}
		Pair<Integer,Integer> goal = mBoat.getFreeSeat();
		if(goal == null) {
			JOptionPane.showMessageDialog(null, "Kein freier Sitz verfuegbar!", "Fehler", JOptionPane.ERROR_MESSAGE);
		} else {
			move(person, goal);
			mBoat.setSeat(goal);
		}
	}
	
	// Bewegt Person und Boot ueber den Fluss
	protected void sailOver(Person person) {
		if(mBoat.getStatus() == MyObject.LEFT) {
			while (!mBoat.getActual().getFirst().equals(mBoat.getRight().getFirst())) {
				move_step(mBoat, mBoat.getRight());
				move_step(person, BANK_RIGHT);
				refresh();
			}
			mBoat.setStatus(MyObject.RIGHT);
		} else {
			while (!mBoat.getActual().getFirst().equals(mBoat.getLeft().getFirst())) {
				move_step(mBoat, mBoat.getLeft());
				move_step(person, BANK_LEFT);
				refresh();
			}
			mBoat.setStatus(MyObject.LEFT);
		}
	}
	
	// Bewegt 2 Personen mit dem Boot ueber den Fluss
	protected void sailOver(Person p1, Person p2) {
		if(mBoat.getStatus() == MyObject.LEFT) {
			while (!mBoat.getActual().getFirst().equals(mBoat.getRight().getFirst())) {
				move_step(mBoat, mBoat.getRight());
				move_step(p1, BANK_RIGHT);
				move_step(p2, BANK_RIGHT);
				refresh();
			}
			mBoat.setStatus(MyObject.RIGHT);
		} else {
			while (!mBoat.getActual().getFirst().equals(mBoat.getLeft().getFirst())) {
				move_step(mBoat, mBoat.getLeft());
				move_step(p1, BANK_LEFT);
				move_step(p2, BANK_LEFT);
				refresh();
			}
			mBoat.setStatus(Boat.LEFT);
		}
	}
	
	// Bewegt die Person vom Boot zum Ufer
	protected void offBoat(Person person) {
		if (person.getStatus() == MyObject.LEFT) {
			move(person, BANK_RIGHT);
			move(person, person.mRight);
			person.setStatus(MyObject.RIGHT);
		} else {
			move(person, BANK_LEFT);
			move(person, person.mLeft);
			person.setStatus(MyObject.LEFT);
		}
	}
	
	// Bringt Person von einer Seite zur anderen
	protected void toOtherSide(Person p) {
		p.setChosen(false);
		toBoat(p);
		sailOver(p);
		offBoat(p);
		mBoat.unsetSeats();
	}
	
	// Bringt 2 Person von einer Seite zur anderen
	protected void toOtherSide(Person p1, Person p2) {
		p1.setChosen(false);
		p2.setChosen(false);
		toBoat(p1);
		toBoat(p2);
		sailOver(p1, p2);
		offBoat(p1);
		offBoat(p2);
		mBoat.unsetSeats();
	}
	
	// stellt einen kompletten Uebergang dar
	protected void step_on() {
		if (this.count == mPath.size()) {
			return;
		}
		
		SetTriple last_triple = mPath.get(this.count-1);
		SetTriple triple = mPath.get(this.count);
		
		int status = mBoat.getStatus();				
		Pair<Integer, Integer> diff = last_triple.difference(triple);
		int number_of_miss = Math.abs(diff.getFirst());
		int number_of_kan = Math.abs(diff.getSecond());
				
		if (number_of_miss + number_of_kan == 1) {
			if (number_of_miss == 1) {
				toOtherSide(getPerson(status, MyObject.MISS));
			} else {
				toOtherSide(getPerson(status, MyObject.KANN));
			}
		} else {
			if (number_of_miss == 2) {
				toOtherSide(getPerson(status, MyObject.MISS), getPerson(status, MyObject.MISS));
			} else if (number_of_kan == 2) {
				toOtherSide(getPerson(status, MyObject.KANN), getPerson(status, MyObject.KANN));
			} else {
				toOtherSide(getPerson(status, MyObject.MISS), getPerson(status, MyObject.KANN));
			}
		}
		this.count++;
		
		if (this.count == mPath.size()) {
			mutate();
			delay(500);
			JOptionPane.showMessageDialog(null, "Die Kannibalen wurden bekehrt!", "Herzlichen Glueckwunsch", JOptionPane.INFORMATION_MESSAGE);
		}
	}
	
	// findet eine verfuegbare Person
	protected Person getPerson(int status, char type) {
		if (type == MyObject.MISS) {
			for (int i=0; i<3; i++) {
				if (mMiss[i].getStatus() == status  && !mMiss[i].isChosen()) {
					mMiss[i].setChosen(true);
					return mMiss[i];
				}
			}
		} else {
			for (int i=0; i<3; i++) {
				if (mKann[i].getStatus() == status  && !mKann[i].isChosen()) {
					mKann[i].setChosen(true);
					return mKann[i];
				}
			}
		}
		return null;
	}
	
	// Veraendert das Aussehen der Kannibale, wenn alles gut gelaufen ist
	protected void mutate() {
		delay(200);
		for (int i=250; i>0; i=i-10) {
			for (int j=0; j<mKann.length; j++) {
				if ( mKann[j].getSource().equals(Sources.GIF_KANN)) {
					mKann[j].load_Image(Sources.GIF_KANN_ANGEL);
				} else {
					mKann[j].load_Image(Sources.GIF_KANN);
				}
			}
			refresh();
			delay(i);
		}
		delay(250);
		for(int i=0; i<mKann.length; i++) {
			Pair<Integer, Integer> goal = new Pair<Integer, Integer>(mKann[i].getActual().getFirst(),-150);
			moveToHeaven(mKann[i], goal);
		}
	}
	
	// verzoegert um die gewuenschte Zeit
	protected void delay(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException ignored) {
			ignored.printStackTrace();
		}
	}
	
	// Zeichnt das DrawPanel neu
	protected final void refresh() {
		mPanel.repaint();
		try {
			Thread.sleep(DELAY);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}


	//********************** Fuer das Arbeiten mit Buttons **********************//
	
	// stellt den Ausgangszustand wieder her
	protected void reset() {
		mView = null;
		for (int i=0; i<3; i++) {
			mMiss[i].setActual(mMiss[i].getLeft());
			mMiss[i].setStatus(MyObject.LEFT);
			mKann[i].setActual(mKann[i].getLeft());
			mKann[i].setStatus(MyObject.LEFT);
		}
		mBoat.setActual(mBoat.getLeft());
		mBoat.setStatus(Person.LEFT);
		this.count = 1;
		mPanel.repaint();
	}
	
	// run()-Methode des Threads mView
	public void run() {
		if (!this.running) {
			step_on();
		}
		if (this.running){
			for(int i=this.count; i<mPath.size() && this.loop; i++) {
				step_on();
			}
		}
		this.reset.setEnabled(true);
	}

	// actionPerformed()-Methode des ActionListeners
	public void actionPerformed(ActionEvent e) {
		String command =e.getActionCommand();
		if (command.equals("Step")) {
			this.running = false;
			this.loop = false;
			this.reset.setEnabled(false);
			if (mView == null || !mView.isAlive()) {
				mView = new Thread(this);
				mView.start();
			}
		}
		if (command.equals("Run")) {
			this.running = true;
			this.loop = true;
			this.reset.setEnabled(false);
			if (mView == null || !mView.isAlive()) {
				mView = new Thread(this);
				mView.start();
			}
		}
		if (command.equals("Stop")) {
			this.loop = false;
		}
		if (command.equals("Reset")) {
			mView = null;
			reset();
		}
		if (command.equals("End")) {
			System.exit(0);
		}
		if (e.getSource() instanceof JTextField) {
			DELAY = Integer.parseInt(delayField.getText());
		}
	}
}
