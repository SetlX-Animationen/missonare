public class Triple<M extends Comparable<? super M>, 
                  K extends Comparable<? super K>,
                  B extends Comparable<? super B>> 
    implements Comparable<Triple<M,K,B>>
{
    M mFirst;
    K mSecond;
    B mThird;
    
    public Triple(M first, K second, B third) {
        mFirst  = first;
        mSecond = second;
        mThird = third;
    }

    public int compareTo(Triple<M, K, B> triple) {
        int cmpFirst = mFirst.compareTo(triple.getFirst());
        if (cmpFirst < 0 || cmpFirst > 0) {
            return cmpFirst;
        }
        int cmpSecond = mSecond.compareTo(triple.getSecond());
        if (cmpSecond < 0 || cmpSecond > 0) {
            return cmpSecond;
        }
        return mThird.compareTo(triple.getThird());
    }

    public String toString() {
        return "<" + mFirst + ", " + mSecond + ", " + mThird + ">";
    }

    public M getFirst()  { return mFirst;  }
    public K getSecond() { return mSecond; }
    public B getThird() { return mThird; }

    public void setFirst(M first) { 
        mFirst = first; 
    }
    public void setSecond(K second) { 
        mSecond = second; 
    }
    public void setThird(B third) { 
        mThird = third; 
    }
}
