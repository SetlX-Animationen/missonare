import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;


public class MyObject {
	
	// Statuskonstanten (links, rechts)
	public static final int LEFT = -1;
	public static final int RIGHT = 1;
	
	// Typkonstanten (Kannibale, Missionar, Boot)
	public static final char MISS = 'M';
	public static final char KANN = 'K';
	public static final char BOAT = 'B';
	
	// Bild
	String mSrc;
	Image mImage;
	
	// Positionskoordinaten: Rechts, Links, Aktuell
	public Pair<Integer,Integer> mLeft;
	public Pair<Integer,Integer> mRight;
	public Pair<Integer,Integer> mActual;
	
	// Aktueller Status (links, rechts)
	public int mStatus;
	
	
	// Methoden zum Veraendern und Abfragen des Status
	public int getStatus() {
		return mStatus;
	}
	public void setStatus(int status) {
		mStatus = status;
	}
	
	// Methoden zum Veraendern und Abfragen der aktuellen Position
	public Pair<Integer,Integer> getActual() {
		return mActual;
	}
	public void setActual(Pair<Integer,Integer> pos) {
		mActual = pos;
	}
	public void setActual(int x, int y) {
		mActual = new Pair<Integer, Integer>(x,y);
	}
	
	public Pair<Integer, Integer> getLeft() {
		return mLeft;
	}
	public Pair<Integer, Integer> getRight() {
		return mRight;
	}
	
	// Methoden zum Handling der Bilder
	public Image getImage() {
		return mImage;
	}
	public void load_Image(String s) {
		try {
			mSrc = s;
			mImage = ImageIO.read(new File(s));
		} catch (IOException ignored) {
			ignored.printStackTrace();
		}
	}
	public void load_Image(Image img) {
		mImage = img;
	}
	public String getSource() {
		return mSrc;
	}
}
