import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;


@SuppressWarnings("serial")
public class DrawPanel extends JPanel {
	
	// Personen und Boat werden erzeugt
	protected Missionar[] mMiss = new Missionar[] { new Missionar(135,290,725,290,Person.LEFT,Sources.GIF_MISS),
													new Missionar(135,430,725,430,Person.LEFT,Sources.GIF_MISS),
													new Missionar(135,570,725,570,Person.LEFT,Sources.GIF_MISS)
												  };
	
	protected Kannibale[] mKann = new Kannibale[] { new Kannibale(0,290,860,290,Person.LEFT,Sources.GIF_KANN),
													new Kannibale(0,430,860,430,Person.LEFT,Sources.GIF_KANN),
													new Kannibale(0,570,860,570,Person.LEFT,Sources.GIF_KANN)
												  };
	
	protected Boat mBoat = new Boat(275,490,475,490,395,430,290,430,Boat.LEFT,Sources.GIF_BOAT);
	
	// Bild der Landschaft
	protected Image landscape;
	
	// Zwei Zeilen, mit denen man Text ausgeben kann
	protected String mStr1 = "";
	protected String mStr2 = "";
	
	// Konstruktor
	public DrawPanel(int width, int height) {
		setSize(width, height);
		
		try {
			this.landscape = ImageIO.read(new File(Sources.JPG_LANDSCAPE));
		} catch (IOException e) {
			this.landscape = null;
			e.printStackTrace();
		}
	}

	// zeichnet alle Komponenten
	public void paintComponent(Graphics g) {
		
		Graphics2D g2 = (Graphics2D) g;
		super.paintComponent(g);
		
		
		// zeichne Landschaft
		g2.drawImage(this.landscape, 0, 0, null);
		
		
		// zeichne Personen
		for(int i=0; i<3; i++) {
			g2.drawImage(mMiss[i].getImage(), mMiss[i].getActual().getFirst(), mMiss[i].getActual().getSecond(), null);
			g2.drawImage(mKann[i].getImage(), mKann[i].getActual().getFirst(), mKann[i].getActual().getSecond(), null);
		}
		
		
		// zeichne Boot
		g2.drawImage(mBoat.getImage(), mBoat.getActual().getFirst(), mBoat.getActual().getSecond(), null);
		
		
		// zeichne Text
		FileInputStream fis;
		Font font;
		try {
			fis = new FileInputStream(Sources.TTF_TIMES_NEW_ROMAN);
			font = Font.createFont(Font.TRUETYPE_FONT, fis);
			font = font.deriveFont(Font.BOLD, 20.0F);
			g2.setFont(font);
		} catch (Exception ignored) {
			ignored.printStackTrace();
		}
		g2.drawString(mStr1, 700, 375);
		g2.drawString(mStr2, 750, 400);
	}
}
