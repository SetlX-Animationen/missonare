
public class Missionar extends Person {
	
	Missionar(Pair<Integer,Integer> left, Pair<Integer,Integer> right, int status, String img) {
		super(left, right, status, img);
	}
	
	Missionar(int x1, int y1, int x2, int y2, int status, String img) {
		super(x1, y1, x2, y2, status, img);
	}
	
	// Typ des Objektes --> Missionar
	public static char getType() {
		return Person.MISS;
	}
}
