public class Missionare 
{    
    public static void main(String args[]) {
    	
    	// P := { [ M, K, B ] : M in {0..3}, K in {0..3}, B in {0,1} |
    	// not problem(M, K) and not problem(3 - M, 3 - K) };
    	
        PointSet p = new PointSet();
        for (Integer m: ComparableSet.range(0,3)) {
            for (Integer k: ComparableSet.range(0,3)) {
            	for (Integer b: ComparableSet.range(0,1)) {
            		if (!problem(m,k) && !problem(3-m,3-k)) {
            			p.add(new SetTriple(m, k, b));
            		}
            	}
            }
        }
    	
        System.out.println("\np:\n"+p);        
		PointRelation r = new PointRelation();
		
		// R1 := { [ [ M, K, 1 ], [ M - PM, K - PK, 0 ] ] : [ M, K, B ] in P, PM in {0..2}, PK in {0..2} |
        // PM + PK in {1,2}  -- Das Boot darf nicht leer sein.
        // and  PM + PK <= 2 -- Das Boot fasst maximal zwei Passagiere.
        // and  PM <= M      -- Es koennen nicht mehr Missionare uebersetzen, als Missionare am linken Ufer sind
        // and  PK <= K      -- Es koennen nicht mehr Kannibalen uebersetzen, als Kannibalen am linken Ufer sind
        // and  [ M - PM, K - PK, 0 ] in P -- Der neue Zustand muss in der Menge aller moeglichen Zustaende liegen.
        //                                 -- Dadurch wird sichergestellt, dass es hinterher keine Probleme gibt.
        // };
		for (SetTriple mkb1: p) {
			for (Integer pm: ComparableSet.range(0,2)) {
				for (Integer pk: ComparableSet.range(0,2)) {
					Integer m = mkb1.getFirst();
					Integer k = mkb1.getSecond();
					Integer sum = pm + pk;
					SetTriple mkb0 = new SetTriple(m-pm, k-pk, 0);
					if(sum>0 && sum<=2 && pm<=m && pk<=k && p.member(mkb0)) {
						mkb1 = new SetTriple(m, k, 1);
						r.add(new Pair<SetTriple, SetTriple>(mkb1, mkb0));
					}
				}
			}
		}
		
		// R2 := { [ [ M, K, 0 ], [ M + PM, K + PK, 1 ] ] : [ M, K, B ] in P, PM in {0..2}, PK in {0..2} |
        // PM + PK >= 1  -- Das Boot darf nicht leer sein.
        // and  PM + PK <= 2 -- Das Boot fasst maximal zwei Passagiere.
        // and  PM <= 3 - M      -- Es koennen nicht mehr Missionare uebersetzen, als Missionare am linken Ufer sind
        // and  PK <= 3 - K      -- Es koennen nicht mehr Kannibalen uebersetzen, als Kannibalen am linken Ufer sind
        // and  not problem(M + PM, K + PK) -- Es darf am linken Ufer kein Problem geben.
        // not problem(3 - M - PM, 3 - K - PK) -- Es darf am rechten Ufer kein Problem geben.
        // };
		for (SetTriple mkb1: p) {
			for (Integer pm: ComparableSet.range(0,2)) {
				for (Integer pk: ComparableSet.range(0,2)) {
					Integer m = mkb1.getFirst();
					Integer k = mkb1.getSecond();
					Integer sum = pm + pk;
					SetTriple mkb0 = new SetTriple(m+pm, k+pk, 1);
					if(sum>=1 && sum<=2 && pm<=3-m && pk<=3-k && !problem(m+pm,k+pk) && !problem(3-m-pm,3-k-pk)) {
						mkb1 = new SetTriple(m, k, 0);
						r.add(new Pair<SetTriple, SetTriple>(mkb1, mkb0));
					}
				}
			}
		}
				
		System.out.println("Relation R:");
		for (Pair<SetTriple, SetTriple> xy : r) {
			System.out.println(xy);
		}
		
		SetTriple start = new SetTriple(3, 3, 1);
		SetTriple goal  = new SetTriple(0, 0, 0);
		Relation<SetTriple> relation = new Relation<SetTriple>(r);
		ComparableList<SetTriple> path = relation.findPath(start, goal);
		System.out.println("\n\nLoesung:\n");
		for (SetTriple triple : path) {
			System.out.println(triple);
		}
		@SuppressWarnings("unused")
		AnimationFrame2 animation = new AnimationFrame2(path);
    }

    // return M > 0 and M < K;
    public static boolean problem(Integer m, Integer k) {
		return (m>0 && m<k);
	}
}

// Alias-Klassen

class SetTriple extends Triple<Integer, Integer, Integer> 
{
	SetTriple(Integer m, Integer k, Integer b) {
		super(m, k, b);
	}
	
	public Pair<Integer, Integer> difference(SetTriple triple) {
		int m = mFirst - triple.mFirst;
		int k = mSecond - triple.mSecond;
		return new Pair<Integer, Integer>(m,k);
	}
}

class PointSet extends ComparableSet<SetTriple> 
{
    PointSet() {
        super();
    }
}

class PointRelation extends ComparableSet<Pair<SetTriple, SetTriple>> 
{
	PointRelation() {
		super();
	}
}

    
