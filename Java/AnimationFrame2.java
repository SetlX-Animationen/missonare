import java.awt.event.ActionEvent;
import java.io.File;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.swing.JOptionPane;


@SuppressWarnings("serial")
public class AnimationFrame2 extends AnimationFrame {
	
	public AnimationFrame2(ComparableList<SetTriple> path) {
		super(path);
		count = 0;
	}
	
	// Ablauf, wenn ein Missionar getoetet wird
	public void kill_kenny() {
		// 2 Missionare gehen zur anderen Seite
		toOtherSide(mMiss[1],mMiss[2]);
		delay(100);
		
		// Kannibale lachen
		changeImage(Sources.GIF_KANN_LAUGH, Kannibale.getType(), 0, 3, 1500);
		// 2 Kannibale gehen hinter den Missionar 
		move(mKann[0], new Pair<Integer, Integer>(50,215));
		move(mKann[1], new Pair<Integer, Integer>(225,215));
		delay(50);
		// Missionar schaut zuerst verlegen, dann schockiert
		changeImage(Sources.GIF_MISS_LOOK_LEFT, Missionar.getType(), 0, 1, 1500);
		changeImage(Sources.GIF_MISS_LOOK_RIGHT, Missionar.getType(), 0, 1, 1500);
		changeImage(Sources.GIF_MISS_SHOCKED, Missionar.getType(), 0, 1, 500);
		// 3. Kannibale dreht sich um und toetet Missionar
		changeImage(Sources.GIF_KANN_BACK, Kannibale.getType(), 2, 3, 250);
		move(mKann[2], new Pair<Integer,Integer>(100,290));
		move(mKann[2], new Pair<Integer,Integer>(170,290));
		mKann[2].setStatus(MyObject.RIGHT);
		move(mKann[2], new Pair<Integer,Integer>(115,435));
		delay(250);
		// Missionar stirbt
		changeImage(Sources.GIF_MISS_DEAD_1, Missionar.getType(), 0, 1, 500);
		changeImage(Sources.GIF_MISS_DEAD_2, Missionar.getType(), 0, 1, 500);
		changeImage(Sources.GIF_MISS_DEAD_3, Missionar.getType(), 0, 1, 1000);
		changeImage(Sources.GIF_MISS_DEAD_4, Missionar.getType(), 0, 1, 1000);
		
		// die anderen Missionare sind geschocked
		changeImage(Sources.GIF_MISS_SHOCKED, Missionar.getType(), 1, 3, 250);
		// Ausgabe von Ton und Text
		Clip clip = loadSound(Sources.WAV_OMG);
		clip.start();
		print(Sources.STRING_OMG_1, null, 1100);
		print(Sources.STRING_OMG_2, null, 400);
		print(Sources.STRING_OMG_3, null, 400);
		print(Sources.STRING_OMG_4, null, 700);
		print(Sources.STRING_OMG_4, Sources.STRING_OMG_5, 1500);
		clip.stop();
		
		// Fertig! Reset-Button druecken zum fortfahren
		this.step.setEnabled(false);
		this.run.setEnabled(false);
		this.stop.setEnabled(false);
		
		delay(1000);
		JOptionPane.showMessageDialog(null, "Die Kannibalen haben Kenny getoetet!\n Versuchen Sie es erneut!", "Oh je ...", JOptionPane.INFORMATION_MESSAGE);
	}
	
	// gibt einen 2-zeiligen Text aus
	protected void print(String s1, String s2, int delay) {
		if (s1 == null) {
			s1 = "";
			mPanel.mStr2 = s2;
		} else if (s2 == null) {
			mPanel.mStr1 = s1;
			s2 = "";
		} else {
			mPanel.mStr1 = s1;
			mPanel.mStr2 = s2;
		}
		refresh();
		delay(delay);
	}
	
	// ladet einen Sound
	protected Clip loadSound(String src) {
		Clip clip = null;
		try {
			AudioInputStream ais = AudioSystem.getAudioInputStream(new File(src));
			DataLine.Info info = new DataLine.Info(Clip.class, ais.getFormat());
			clip = (Clip) AudioSystem.getLine(info);
			clip.open(ais);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return clip;
	}
	
	// aendert das Aussehen einer Person
	protected void changeImage(String src, char type, int start, int times, int delay) {
		if(type == Missionar.getType()) {
			for (int i=start; i<times; i++) {
				mPanel.mMiss[i].load_Image(src);
			}
		} else {
			for (int i=start; i<times; i++) {
				mPanel.mKann[i].load_Image(src);
			}
		}
		refresh();
		delay(delay);
	}
	
	// stellt sicher, dass Kenny nur einmal am Anfang getoetet wird
	// beim naechsten mal funktioniert das Programm "richtig"
	protected void step_on() {
		if (this.count == 0) {
			kill_kenny();
			this.count++;
			return;
		} else {
			super.step_on();
		}
	}
	
	// ueberschriebene reset()-Methode
	protected void reset() {
		this.step.setEnabled(true);
		this.run.setEnabled(true);
		this.stop.setEnabled(true);
		for (int i=0; i<3; i++) {
			mPanel.mMiss[i].load_Image(Sources.GIF_MISS);
			mPanel.mKann[i].load_Image(Sources.GIF_KANN);
		}
		mPanel.mStr1 = "";
		mPanel.mStr2 = "";
		super.reset();
	}
	
	// ueberschriebene actionPerformed()-Methode
	public void actionPerformed(ActionEvent e) {
		String command =e.getActionCommand();
		if (command.equals("Run") && this.count == 0) {
			this.running = false;
			this.loop = false;
			this.reset.setEnabled(false);
			if (mView == null || !mView.isAlive()) {
				mView = new Thread(this);
				mView.start();
			}
		} else {
			super.actionPerformed(e);
		}
	}
	
}
