public class Boat extends MyObject {
	
	// Koordinaten der Sitze (links, rechts)
	private Pair<Integer, Integer> mLeft_Seat_Front;
	private Pair<Integer, Integer> mLeft_Seat_Back;
	private Pair<Integer, Integer> mRight_Seat_Front;
	private Pair<Integer, Integer> mRight_Seat_Back;
	
	// Status der Sitze (belegt, nicht belegt)
	private boolean mFront_Seat_Empty = true;
	private boolean mBack_Seat_Empty = true;
	
	// Laenge der ueberfahrt mit dem Boot
	private int mDistance;

	
	
	public Boat(Pair<Integer, Integer> left,
				Pair<Integer, Integer> right,
				Pair<Integer, Integer> seat_front,
				Pair<Integer, Integer> seat_back,
				int status,
				String img) {

		mLeft = left;
		mRight = right;
		mDistance = right.getFirst()-left.getFirst();
		mStatus = status;
		load_Image(img);

		if (status == LEFT)	{
			mLeft_Seat_Front = seat_front;
			mLeft_Seat_Back = seat_back;
			mRight_Seat_Front = new Pair<Integer, Integer>(seat_back.getFirst() + mDistance, seat_back.getSecond());
			mRight_Seat_Back = new Pair<Integer, Integer>(seat_front.getFirst() + mDistance, seat_front.getSecond());
			mActual = left;
		}

		if (status == RIGHT) {
			mLeft_Seat_Front = new Pair<Integer, Integer>(seat_front.getFirst() - mDistance, seat_front.getSecond());
			mLeft_Seat_Back = new Pair<Integer, Integer>(seat_back.getFirst() - mDistance, seat_back.getSecond());
			mRight_Seat_Front = seat_front;
			mRight_Seat_Back = seat_back;
			mActual = right;
		}
	}

	public Boat(int left_x, int left_y,
				int right_x, int right_y,
				int seat_front_x, int seat_front_y,
				int seat_back_x, int seat_back_y,
				int status,
				String img) {
		
		this(new Pair<Integer, Integer>(left_x, left_y),
			 new Pair<Integer, Integer>(right_x, right_y),
			 new Pair<Integer, Integer>(seat_front_x, seat_front_y),
			 new Pair<Integer, Integer>(seat_back_x, seat_back_y),
			 status,
			 img);
	}

	
	// gibt die Koordinaten eines freien Sitzes zurueck
	public Pair<Integer, Integer> getFreeSeat() {

		if (mStatus == LEFT) {
			if (mFront_Seat_Empty) {
				return mLeft_Seat_Front;
			} else if (mBack_Seat_Empty) {
				return mLeft_Seat_Back;
			} else {
				return null;
			}
		} else {
			if (mFront_Seat_Empty) {
				return mRight_Seat_Front;
			} else if (mBack_Seat_Empty) {
				return mRight_Seat_Back;
			} else {
				return null;
			}
		}
	}

	// setzt einen Sitz auf belegt
	public void setSeat(Pair<Integer, Integer> seat) {
		if ((seat.equals(mLeft_Seat_Front)) || (seat.equals(mRight_Seat_Front))) {
			mFront_Seat_Empty = false;
		} else if ((seat.equals(mLeft_Seat_Back)) || (seat.equals(mRight_Seat_Back)))	{
			mBack_Seat_Empty = false;
		}
	}

	// setzt einen Sitz auf belegt
	public void setSeat(int x, int y) {
		setSeat(new Pair<Integer, Integer>(x, y));
	}
	
	// gibt sSitze wieder frei
	public void unsetSeats() {
		mFront_Seat_Empty = true;
		mBack_Seat_Empty = true;
	}
	
	// Typ des Objektes --> Boot
	public char getType() {
		return MyObject.BOAT;
	}
}