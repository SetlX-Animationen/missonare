
public class Person extends MyObject {
	
	// Gibt an, ob die Person ausgewählt ist
	boolean mChosen;
		
	public Person(Pair<Integer,Integer> left, Pair<Integer,Integer> right, int status, String img) {
		mLeft = left;
		mRight = right;
		if(status == Person.LEFT) {
			mActual = mLeft;
			mStatus = Person.LEFT;
		} else {
			mActual = mRight;
			mStatus = Person.RIGHT;
		}
		load_Image(img);
	}
	
	public Person(int x1, int y1, int x2, int y2, int status, String img) {
		this(new Pair<Integer,Integer>(x1, y1), new Pair<Integer,Integer>(x2, y2), status, img);
	}
	
	public boolean isChosen() {
		return mChosen;
	}
	
	public void setChosen(boolean chosen) {
		mChosen = chosen;
	}
}
